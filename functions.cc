#include "header.h"
using namespace std;

// Verifica se l'albero � vuoto
bool test_vuoto()
{
	return root ? true : false;
}

// Richiama la funzione ricorsiva ric_tree()
void richiamaCostr(void)
{
    // Apertura del file contenente le domande
    readfile.open("questions.txt", ios::in);

    char par[BUFFER] = "Si trova in centro storico?";
    ric_tree(par);

    // Chiusura del file
    readfile.close();
}

// Costruisce l'abero binario
node* ric_tree(string question)
{
	if (readfile.is_open()) {
        node* newSubTree = new node; // ogni volta che la funzione viene richiamata si crea un nuovo nodo
        /*	first -> primo carattere della stringa della domanda
         * 	newSubTree -> nodo del sottoalbero */

        // Se � una foglia
        if (question[0] != '@') {
            // La domanda viene inserita nel nodo
            newSubTree->str = question;

            // Richiamando la funzione ricorsiva si riescono a costruire i sotto alberi destro e sinistro rispetto alla radice
            getline(readfile, question);
            newSubTree->dx = ric_tree(question);
            getline(readfile, question);
            newSubTree->sx = ric_tree(question);
            root = newSubTree;
        }
        else {
            // La domanda viene inserita nel nodo
            newSubTree->str = question;

            // I puntatori al sottoablbero destro e al sottoablbero sinitro puntano a NULL
            newSubTree->sx = NULL;
            newSubTree->dx = NULL;

            return newSubTree; // restituisce il sotto-albero
        }
	}
	else {
		int mb_result; // risposta utente alla finestra
		mb_result = MessageBox(NULL, "Impossibile caricare le domande\nControllare che il file questions.txt sia presente nella cartella del programma", "Errore", MB_OK | MB_ICONERROR);
	}
}

// Funzione che permette di giocare al gioco
bool play(void)
{
    node* nod = root; // q punta alla radice dell'albero

	// Scorre tutto l'albero binario in base alle risposte dell'utente
    while (nod) {
    	// Se il nodo in questione fosse una foglia, si stampa a schermo la soluzione, se il computer ha indovinato si chiude il processo, altrimenti 
		if (nod->str[0] == '@') {
			nod->str[0] = ' '; // Il primo carattere, per capirci l'at delimitatorio, viene sostituito con un carattere nullo
			
			int mb_result = MessageBox(NULL, nod->str.c_str(), "Stai pensando a..", MB_YESNO | MB_ICONQUESTION); // risposta utente alla finestra
			
			switch (mb_result) {
				case (IDYES): {
					::MessageBox(NULL, "Sono bravissimo!", "Ho vinto anche a questa partita", MB_OK | MB_ICONASTERISK);
					
					// Chiusura del processo aperto
					system("taskkill /f /im main.exe");
					
					break;
				}
				case (IDNO): {
					return false;
					break;
				}
			}
		}
		// Domanda random
		else if (nod->str[0] == '#') {
			// Il primo carattere, per capirci l'hastag delimitatorio, viene sostituito con un carattere nullo
			nod->str[0] = ' ';
			string domandaRandom = random();
			
			int mb_result = MessageBox(NULL, domandaRandom.c_str(), "Ti far� qualche domanda", MB_YESNO | MB_ICONQUESTION);
			
        	switch (mb_result) {
	        	case (IDYES): {
	        		nod = nod->dx;
					break;
				}
				case (IDNO): {
					nod = nod->sx;
					break;
				}
			}
		}
		// Se il nodo in questione non fosse una foglia
		else {
			int mb_result = MessageBox(NULL, nod->str.c_str(), "Ti far� qualche domanda", MB_YESNO | MB_ICONQUESTION);
        	switch (mb_result) {
	        	case (IDYES): {
	        		nod = nod->dx;
					break;
				}
				case (IDNO): {
					nod = nod->sx;
					break;
				}
    		}
    	}
    }
}

// Genera una domanda a caso
string random(void)
{
	int nRandom = rand() % 31;
	string domanda;
	
	ifstream randomQuestions;
	randomQuestions.open("random.txt", ios::in);
	
	if (randomQuestions.is_open()) {
		getline(randomQuestions, domanda);
		for (int i = 0; i < nRandom; i++) {
			getline(randomQuestions, domanda);
		}
		return domanda;
	}
	else {
		::MessageBox(NULL, "Nella cartella d'installazione manca il file 'random.txt'", "Errore!", MB_OK | MB_ICONWARNING);
		system("taskkill /f /im main.exe");
	}
}

// Nasconde la finestra del terminale
void Stealth()
{
	HWND Stealth;
	AllocConsole();
	Stealth = FindWindowA("ConsoleWindowClass", NULL);
	ShowWindow(Stealth,0);
}
