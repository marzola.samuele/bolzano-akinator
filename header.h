#ifndef HEADER_H_INCLUDED
#define HEADER_H_INCLUDED

#include <iostream>
#include <fstream>
#include <windows.h>
#include <stdio.h>      /* printf, scanf, puts, NULL */
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include <string.h>
using namespace std;

/** STRUCT **/
struct node {
	string str;
	node *pad;
	node *dx;
	node *sx;
} NODE;
node* root = NULL;
/*rad - domanda o luogo da indovinare
  *pad - puntatore al padre del nodo
  *sx - sotto albero sinistro
  *dx - sotto albero destro
  *first - puntatore alla radice */

/** FUNCTIONS **/
bool test_vuoto(void);
bool play(void);
void clear(void);
void saveFile(string question, node q);
string random(void);
node* ric_tree(string question);
node* void_tree (void);

/** VARIABILI GLOBALI **/
ifstream readfile;
#define LIM 250
#define BUFFER 200

#endif // HEADER_H_INCLUDED
