/* AREA DI PROGETTO 2016 - ALTIERI DAVID, MARZOLA SAMUELE
INIZIO: 16 apr 2016 */
#include <windows.h>
#include "header.h"
#include "functions.cc"
using namespace std;

/* Dichiarazione Windows procedure */
LRESULT CALLBACK WindowsProcedure(HWND, UINT, WPARAM, LPARAM);
const char szClassName[] = "myWindowClass";
char textSaved[20]; // contiene il luogo che il programma non � riuscito a indovinare
HWND TextBox;

// Procedura della finestra
LRESULT CALLBACK WndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    switch (msg) {
		// Creazione della finetsra per l'inserimento del luogo che il programma non � riuscito ad indovinare:
    	case (WM_CREATE): {
			TextBox = CreateWindow( "EDIT",
									"Inserisci il luogo!",
									WS_BORDER | WS_CHILD | WS_VISIBLE,
									10, 10, 200, 20,
									hwnd, NULL, NULL, NULL);
			CreateWindow("BUTTON",
						 "Invio",
						 WS_VISIBLE | WS_CHILD | WS_BORDER,
						 220, 10, 70, 20,
						 hwnd, (HMENU) 1, NULL, NULL);

			break;
		}
		case (WM_COMMAND): {
			switch(LOWORD(wParam)) {
				case (1): {
					int gwtstat = 0;
					gwtstat = GetWindowText(TextBox, &textSaved[0], 20);

					// Se l'utente non dovesse scrivere niente nella casella di testo, si apre una finestra di errore:
					if (gwtstat == 0)
						::MessageBox(hwnd, "Inserire assolutamente un luogo", "Errore", MB_ICONWARNING | MB_OK);
					// Altrimenti gli si dice che il luogo � stato inserito corretamente.
					else {
						::MessageBox(hwnd, "Luogo inserito correttamente", "Congratulazioni", MB_ICONASTERISK | MB_OK);
						// e il programma viene chiuso.
						system("taskkill /f /im main.exe");
					}
					break;
				}
			}
			break;
		}
        case (WM_CLOSE): {
			DestroyWindow(hwnd);
			break;
		}
        case (WM_DESTROY): {
			PostQuitMessage(0);
			break;
		}
        default:
            return DefWindowProc(hwnd, msg, wParam, lParam);
    }
    return 0;
}

// Main
int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	Stealth(); // nascondere il terminale
	srand (time(NULL)); /* initialize random seed: */
	
	char par[200] = "Pensa ad un luogo della citta' di Bolzano\n(Per esempio una scuola, una piazza, un monumento, un centro commerciale, ... )\nQuando ti senti pronto clicca su OK";
	bool indovinato = true;
	int mb_result = MessageBox(NULL, par, "Indovinero' ci� che stai pensando", MB_OKCANCEL | MB_ICONASTERISK);
	/*	mb_result -> variabile usata per il ritorno di valore
		par -> stringa contenente le info della prima finestra */

	// Se l'utente clicca SI alla prima finestra inizia il gioco:
	if (mb_result == IDOK) {
		// L'albero binario viene costruito:
		richiamaCostr();
		
		// Inizia il gioco:
		indovinato = play();

		// Se il programma non ha iondovinato il luogo, si chiede all'utente di inserire il luogo pensato:
		if (!indovinato) {
			HWND hwnd; // handle della finestra
			MSG messages; //  messaggio salvato dalla applicazione
			WNDCLASSEX wincl; // struttura dati per la windows-class

			/* Struttura della finestra */
			//wincl.lpfnWndProc = WindowProcedure;
			wincl.hInstance = hInstance;
			wincl.lpszClassName = szClassName;
			wincl.cbSize = sizeof(WNDCLASSEX);
			wincl.lpfnWndProc = WndProc;

			/* Icone e puntatori muose predifiniti */
			wincl.hIcon = LoadIcon(NULL, IDI_APPLICATION);
			wincl.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
			wincl.hCursor = LoadCursor(NULL, IDC_ARROW);
			wincl.style = 0;
			wincl.lpszMenuName = NULL; // NO menu
			wincl.cbClsExtra = 0; // No extra bytes dopo la classe window
			wincl.cbWndExtra = 0; // struttra dell'istanza della finestra

			/* Colori predefiniti da windows per le finestre */
			wincl.hbrBackground = (HBRUSH)(COLOR_WINDOW+1);

			/* La classe windows viene registrata, se dovesse fallire il programma si chiude */
			if (!RegisterClassEx(&wincl)) {
				MessageBox(NULL, "Registrazione della classe fallita", "Errore!", MB_ICONEXCLAMATION | MB_OK);
				return 0;
			}

			/* La classe � ora registrata, bisogna creare la finestra */
			hwnd = CreateWindowEx(
				0, // Estende la possibilita� di estensioni
				szClassName, // Nome della classe
				"Questa volta hai vinto tu!", // Titolo
				WS_SYSMENU | WS_MINIMIZEBOX, // Finestra classica
				GetSystemMetrics(SM_CXSCREEN)/2-160, // con CW_USEDEFAULT Windows decide la posizione
				GetSystemMetrics(SM_CYSCREEN)/2-70, // con CW_USEDEFAULT Windows decide la posizione
				350, // Larghezza programma
				150, // Altezza programma
				HWND_DESKTOP, // La finestra � una finestra-figlio del desktop
				NULL, // NO menu
				hInstance, // Istanza della maniglia del programma
				NULL // La finestra non crea dati
			);

			if(hwnd == NULL) {
				MessageBox(NULL, "Creazione della finestra fallita", "Errore!", MB_ICONEXCLAMATION | MB_OK);
				return 0;
			}

			ShowWindow(hwnd, nCmdShow);

				// Manda in loop il messaggio
			while (GetMessage(&messages, NULL, 0, 0)) {
				// Traduce il messaggio virtuale in un messaggio di caratteri
				TranslateMessage(&messages);
				// Manda il messaggio alla procedura della finestra
				DispatchMessage(&messages);
			}

			UpdateWindow(hwnd);
			
			// Chiamata alla funzione che salva sul fine il luogo che il programma non � riuscito ad indovinare nella giusta posizione
			//intelligente(textSaved);
		}
		/*else
			return messages.wParam;*/
	}
	// Altrimenti si chiude il programma:
	else 
		system("taskkill /f /im main.exe");
	system("taskkill /f /im main.exe");
}
