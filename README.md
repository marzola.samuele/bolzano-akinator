# Bolzano Akinator

Simple game written in C with graphic interface able to guess the place of the city you are thinking of.
The game is based on the places in the city of Bolzano.
Download the whole package and open the .exe file to play.

Further information: <i>info@samuelemarzola.it</i>

Special thanks to <b>David Altieri</b> for the precious collaboration.


--



Semplice gioco scritto in C con interfaccia grafica in grado di indovinare il luogo della città a cui stai pensando.
Il gioco si basa sui luoghi della città di Bolzano.
Scarica l'intero pacchetto e apri il file .exe per giocare.

Ulteriori informazioni: <i>info@samuelemarzola.it</i>

Un grazie particolare a <b>David Altieri</b> per la preziosa collaborazione.